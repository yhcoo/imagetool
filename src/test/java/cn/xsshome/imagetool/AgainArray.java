package cn.xsshome.imagetool;

import cn.xsshome.imagetool.util.MergeImageUtil;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.List;
import java.util.Random;

/**
 * Description 百叶图
 * ProjectName imagetool
 * Created by 小帅丶 on 2022-06-13 14:15.
 * Version 1.0
 */

public class AgainArray {

    public static String str="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    public static void main(String[] args) throws Exception{

        String sourcePath = "F:\\testfile\\testimg\\";

        String sourceName = "gray_source.jpeg";

        String targetPath = "F:\\testfile\\testimg\\againarray\\";

        List<BufferedImage> bufferedImages = MergeImageUtil.againArrayImage(sourcePath + sourceName, 10);

        for (BufferedImage bufferedImage : bufferedImages) {
            File file = new File(targetPath+System.currentTimeMillis()+getRandomStr()+".jpg");
            ImageIO.write(bufferedImage, "jpg", file);
        }
    }

    public static String getRandomStr() {
        Random random1 = new Random();
        //指定字符串长度，拼接字符并toString
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < 6; i++) {
            int number = random1.nextInt(str.length());
            char charAt = str.charAt(number);
            sb.append(charAt);
        }
        return sb.toString();
    }
}
