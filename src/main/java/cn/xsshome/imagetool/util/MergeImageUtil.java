package cn.xsshome.imagetool.util;


import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName: MergeImageUtil
 * @description: 图片融合(两张图片合并为一张图片)
 * @author: 小帅丶
 * @create: 2019-04-08
 **/
public class MergeImageUtil {
    /**
     * 两张图片合并为一张图片
     * @param srcImage 底图
     * @param pendantImage 挂件图
     * @param x 距离右下角的X偏移量
     * @param y 距离右下角的Y偏移量
     * @param alpha  透明度, 选择值从0.0~1.0: 完全透明~完全不透明
     * @return BufferedImage
     * @throws Exception
     */
    public  static BufferedImage mergePendant(BufferedImage srcImage,BufferedImage pendantImage,int x,int y, float alpha)throws  Exception{
        //创建Graphics2D对象 用在挂件图像对象上绘图
        Graphics2D g2d = srcImage.createGraphics();
        //获取挂件图像的宽高
        int pendantImageWidth = pendantImage.getWidth();
        int pendantImageHeight = pendantImage.getHeight();
        //实现混合和透明效果
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP,alpha));
        //绘制图像
        g2d.drawImage(pendantImage,x,y,pendantImageWidth,pendantImageHeight,null);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        return  srcImage;
    }

    /**
     * @Description 图片进行固定宽度分割再组合成两张新的图片
     * @Author 小帅丶
     * @Date  2022/6/13 17:18
     * @param imagePath - 图片本地路径
     * @param divisionWidth - 分割宽度
     * @return java.util.List<java.awt.image.BufferedImage>
     **/
    public static List<BufferedImage> againArrayImage(String imagePath,int divisionWidth) throws Exception{
        List<BufferedImage> againArrayImage = new ArrayList<>();
        BufferedImage bufferedImage = ImageIO.read(new File(imagePath));
        int width = bufferedImage.getWidth();
        int height = bufferedImage.getHeight();
        int equalDivision = width / divisionWidth;
        List<BufferedImage> oddImage = new ArrayList<>();
        List<BufferedImage> evenImage = new ArrayList<>();
        for (int i = 1; i <= equalDivision-1; i++) {
            int x = i * divisionWidth;
            BufferedImage subImage = bufferedImage.getSubimage(x, 0, divisionWidth, height);
            if(i%2==0){
                evenImage.add(subImage);
            }else{
                oddImage.add(subImage);
            }
        }
        againArrayImage.add( againArray(evenImage));
        againArrayImage.add( againArray(oddImage));
        return againArrayImage;
    }

    /**
     * N张图片合并为一张图片
     *
     * @param bufferedImageList - 图像对象
     * @return BufferedImage
     * @author 小帅丶
     * @date 2022年6月13日
     */
    public static BufferedImage againArray(List<BufferedImage> bufferedImageList) {
        int width = 0;
        int height = bufferedImageList.get(0).getHeight();
        for (BufferedImage bufferedImage : bufferedImageList) {
            width += bufferedImage.getWidth();
        }
        BufferedImage imageNew = new BufferedImage(width + bufferedImageList.size(), height, BufferedImage.TYPE_INT_RGB);
        int wx = 0;
        int wy = 0;
        for (BufferedImage bufferedImage : bufferedImageList) {
            int sw = bufferedImage.getWidth();
            int sh = bufferedImage.getHeight();
            //读取RGB
            int[] imageArrayOne = new int[sw * sh];
            imageArrayOne = bufferedImage.getRGB(0, 0, sw, sh, imageArrayOne,
                    0, sw);
            imageNew.setRGB(wx, 0, sw, sh, imageArrayOne, 0, sw); // 水平方向
            //imageNew.setRGB(0, wy, sw, sh, imageArrayOne, 0, sw); // 垂直方向
            wx += sw + 1;
            wy += sh;
        }
        return imageNew;
    }
}
